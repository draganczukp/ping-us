package me.draganczuk.pingus.server;

import me.draganczuk.pingus.db.ConfigEntity;
import me.draganczuk.pingus.db.ConfigRepository;
import me.draganczuk.pingus.db.ServerEntity;
import me.draganczuk.pingus.db.ServerRepository;
import spark.Request;
import spark.Response;

import static spark.Spark.get;

public class Server {

    private static final ConfigRepository configRepository = new ConfigRepository();
    private static final ServerRepository serverRepository = new ServerRepository();

    public void init() {
	    get("/", (req, res) -> "test");

	    get("/dev/db", this::testDb);
    }

    private String testDb(Request request, Response response) {
        if (System.getenv("DEBUG") == null || System.getenv("DEBUG").isBlank())
            return "Not debugging";

	    try {
		    String serverId = "789100331845812247";

		    ConfigEntity config = ConfigEntity.getDefault();

		    config = configRepository.create(config);

		    ServerEntity server = ServerEntity.getDefault(serverId, config);

		    server.setId(serverRepository.create(server));
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
        return "Done";
    }
}
