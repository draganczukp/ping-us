package me.draganczuk.pingus;

import me.draganczuk.pingus.bot.Bot;
import me.draganczuk.pingus.db.DbAccess;
import me.draganczuk.pingus.server.Server;

public class App {
    public static final Bot BOT = new Bot();
    public static final Server SERVER = new Server();

    public static void main(String[] args) throws Exception {
        DbAccess.init();

        BOT.init();

        SERVER.init();
    }
}
