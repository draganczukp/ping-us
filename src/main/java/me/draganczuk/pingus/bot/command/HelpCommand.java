package me.draganczuk.pingus.bot.command;

import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.Event;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.concurrent.TimeUnit;

public class HelpCommand extends BaseCommand {
	@Override
	public void accept(String[] strings, Event event) {
		MessageReceivedEvent evt = (MessageReceivedEvent) event;

		Message msg = new MessageBuilder()
				.append("Available commands: ")
				.append("\n")
				.append("- help")
				.append("\n")
				.append("- create -- create a new lobby")
				.append("\n")
				.append("- delete -- removes last lobby")
				.append("\n")
				.append("- config -- configure the bot")
				.build();

		evt.getChannel().sendMessage(msg).queue(_msg -> _msg.delete().completeAfter(10, TimeUnit.MINUTES));
		evt.getMessage().delete().queue();
	}
}
