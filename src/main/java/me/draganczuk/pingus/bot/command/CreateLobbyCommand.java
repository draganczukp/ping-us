package me.draganczuk.pingus.bot.command;

import me.draganczuk.pingus.bot.Emotes;
import me.draganczuk.pingus.db.ConfigEntity;
import me.draganczuk.pingus.db.ReactionRepository;
import me.draganczuk.pingus.db.ServerEntity;
import me.draganczuk.pingus.db.ServerRepository;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.Event;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.Optional;

public class CreateLobbyCommand extends BaseCommand {

    private final ServerRepository serverRepository = new ServerRepository();
    private final ReactionRepository reactionRepository = new ReactionRepository();

    @Override
    public void accept(String[] strings, Event event) {
        var evt = (MessageReceivedEvent) event;

        Optional<ServerEntity> server = serverRepository.findByGuildId(evt.getGuild().getId());
        var config = server.map(ServerEntity::getConfig).orElse(ConfigEntity.getDefault());

        server.map(ServerEntity::getMessageId)
                .filter(s -> !s.isBlank())
                .ifPresent(msg -> evt.getChannel().deleteMessageById(msg).queue());

        reactionRepository.clearReactions(evt.getGuild().getId());

        Message msg = new MessageBuilder()
                .mentionRoles(config.getRoleId())
                .append("<@&").append(config.getRoleId()).append(">\n")
                .append("A lobby has been created for today\n")
                .append("Use emotes to choose when you are available\n")
                .append(Emotes.CLOCK0).append(" - Whole day")
                .append("\n")
                .append(Emotes.CLOCK10).append(" - 10:00")
                .append("\n")
                .append(Emotes.CLOCK11).append(" - 11:00")
                .append("\n")
                .append(Emotes.CLOCK12).append(" - 12:00")
                .append("\n")
                .append(Emotes.CLOCK1).append(" - 13:00")
                .append("\n")
                .append(Emotes.CLOCK2).append(" - 14:00")
                .append("\n")
                .append(Emotes.CLOCK3).append(" - 15:00")
                .append("\n")
                .append(Emotes.CLOCK4).append(" - 16:00")
                .append("\n")
                .append(Emotes.CLOCK5).append(" - 17:00")
                .append("\n")
                .append(Emotes.CLOCK6).append(" - 18:00")
                .append("\n")
                .append(Emotes.CLOCK7).append(" - 19:00")
                .append("\n")
                .append(Emotes.CLOCK8).append(" - 20:00")
                .append("\n")
                .append(Emotes.CLOCK9).append(" - 21:00")
                .append("\n")
                .append(Emotes.X).append(" - Not today\n")
                .build();

        evt.getMessage().delete().queue();
        evt.getChannel().sendMessage(msg).queue(message -> {
            message.addReaction(Emotes.CLOCK0).queue();
            message.addReaction(Emotes.CLOCK10).queue();
            message.addReaction(Emotes.CLOCK11).queue();
            message.addReaction(Emotes.CLOCK12).queue();
            message.addReaction(Emotes.CLOCK1).queue();
            message.addReaction(Emotes.CLOCK2).queue();
            message.addReaction(Emotes.CLOCK3).queue();
            message.addReaction(Emotes.CLOCK4).queue();
            message.addReaction(Emotes.CLOCK5).queue();
            message.addReaction(Emotes.CLOCK6).queue();
            message.addReaction(Emotes.CLOCK7).queue();
            message.addReaction(Emotes.CLOCK8).queue();
            message.addReaction(Emotes.CLOCK9).queue();
            message.addReaction(Emotes.X).queue();

            storeMessage(message);
        });
    }

    private void storeMessage(Message message) {
        String messageId = message.getId();
        String guildId = message.getGuild().getId();

        serverRepository.updateMessageId(messageId, guildId);
    }
}
