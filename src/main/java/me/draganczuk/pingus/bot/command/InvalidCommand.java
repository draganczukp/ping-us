package me.draganczuk.pingus.bot.command;

import net.dv8tion.jda.api.events.Event;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class InvalidCommand extends BaseCommand {
    @Override
    public void accept(String[] strings, Event event) {
        var evt = (MessageReceivedEvent) event;
        evt.getChannel().sendMessage("Invalid command: " + strings[0]).queue();
    }
}
