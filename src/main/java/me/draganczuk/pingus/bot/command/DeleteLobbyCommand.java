package me.draganczuk.pingus.bot.command;

import me.draganczuk.pingus.db.ServerEntity;
import me.draganczuk.pingus.db.ServerRepository;
import net.dv8tion.jda.api.events.Event;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.concurrent.TimeUnit;

public class DeleteLobbyCommand extends BaseCommand {
	private final ServerRepository serverRepository = new ServerRepository();

	@Override
	public void accept(String[] strings, Event event) {
		var evt = (MessageReceivedEvent) event;

		evt.getMessage().delete().queue();

		serverRepository.findByGuildId(evt.getGuild().getId())
				.map(ServerEntity::getMessageId)
				.filter(s -> !s.isEmpty())
				.ifPresentOrElse(
						msg -> deleteMessage(evt, msg),
						() -> noLobby(evt)
				);
	}

	private void noLobby(MessageReceivedEvent evt) {
		evt.getChannel().sendMessage("No lobby created")
				.queue(msg -> msg.delete().queueAfter(20, TimeUnit.SECONDS));
	}

	private void deleteMessage(MessageReceivedEvent evt, String msg) {
		evt.getChannel().deleteMessageById(msg).queue();
		serverRepository.clearSavedMessageForGuild(evt.getGuild().getId());
	}
}
