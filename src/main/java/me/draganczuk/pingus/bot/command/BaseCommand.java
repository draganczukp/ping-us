package me.draganczuk.pingus.bot.command;

import net.dv8tion.jda.api.events.Event;

import java.util.function.BiConsumer;

public abstract class BaseCommand implements BiConsumer<String[], Event> {
}
