package me.draganczuk.pingus.bot.command;

import me.draganczuk.pingus.db.ConfigEntity;
import me.draganczuk.pingus.db.ConfigRepository;
import me.draganczuk.pingus.db.ServerEntity;
import me.draganczuk.pingus.db.ServerRepository;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.Event;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class ConfigCommand extends BaseCommand {
    private final ConfigRepository configRepository = new ConfigRepository();
    private final ServerRepository serverRepository = new ServerRepository();

    private final Message usageMessage;

    public ConfigCommand() {
        usageMessage = new MessageBuilder()
                .append("Usage: config [option] [value]")
                .append("\n")
                .append("Available config options: [")
                .append("prefix")
                .append(" ")
                .append("role")
                .append("]")
                .build();
    }

    @Override
    public void accept(String[] strings, Event event) {
        MessageReceivedEvent evt = (MessageReceivedEvent) event;

        var guild = evt.getGuild().getId();

        var config = serverRepository
                .findByGuildId(guild)
                .map(ServerEntity::getConfig)
                .orElse(ConfigEntity.getDefault());

        if (strings.length < 3) {
            evt.getChannel().sendMessage(usageMessage).queue();
            return;
        }

        var key = strings[1];
        var value = strings[2];

        switch (key) {
            case "prefix" -> config.setPrefix(value);
            case "role" -> config.setRoleId(evt.getMessage().getMentionedRoles().get(0).getId());
            default -> evt.getChannel().sendMessage(usageMessage).queue();
        }

        configRepository.update(config);
    }
}
