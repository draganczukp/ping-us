package me.draganczuk.pingus.bot;

import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.User;

import javax.security.auth.login.LoginException;
import java.util.HashMap;
import java.util.HashSet;

public class Bot {

    public static final HashMap<String, HashSet<User>> MAP = new HashMap<>();

    public void init() throws LoginException, InterruptedException {
        var token = System.getenv("TOKEN");
        var msgListener = new MessageListener();
        var reactionListener = new ReactionListener();
        var guildJoinListener = new GuildJoinListener();

        var jda = JDABuilder.createDefault(token)
		        .addEventListeners(msgListener,
				        reactionListener,
				        guildJoinListener)
		        .setActivity(Activity.watching("p!help"))
                .build();
        jda.awaitReady();
        System.out.println("JDA ready");

    }


}
