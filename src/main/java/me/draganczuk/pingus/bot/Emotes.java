package me.draganczuk.pingus.bot;

public class Emotes {
	public static String CLOCK0 = "\u23F0";
	public static String CLOCK1 = "\uD83D\uDD50";
	public static String CLOCK2 = "\uD83D\uDD51";
	public static String CLOCK3 = "\uD83D\uDD52";
	public static String CLOCK4 = "\uD83D\uDD53";
	public static String CLOCK5 = "\uD83D\uDD54";
	public static String CLOCK6 = "\uD83D\uDD55";
	public static String CLOCK7 = "\uD83D\uDD56";
	public static String CLOCK8 = "\uD83D\uDD57";
	public static String CLOCK9 = "\uD83D\uDD58";
	public static String CLOCK10 = "\uD83D\uDD59";
	public static String CLOCK11 = "\uD83D\uDD5A";
	public static String CLOCK12 = "\uD83D\uDD5B";
	public static String X = "\u274C";
}
