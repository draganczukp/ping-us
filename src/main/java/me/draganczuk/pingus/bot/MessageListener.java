package me.draganczuk.pingus.bot;

import me.draganczuk.pingus.bot.command.*;
import me.draganczuk.pingus.db.ConfigEntity;
import me.draganczuk.pingus.db.ConfigRepository;
import me.draganczuk.pingus.db.ServerEntity;
import me.draganczuk.pingus.db.ServerRepository;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

public class MessageListener extends ListenerAdapter {

    private static final String DEFAULT_PREFIX = "!p";
    private final ConfigRepository configRepository = new ConfigRepository();
    private final ServerRepository serverRepository = new ServerRepository();

    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        var server = serverRepository.findByGuildId(event.getGuild().getId());
        var prefix = server
                .map(ServerEntity::getConfig)
                .map(ConfigEntity::getPrefix)
                .orElse(DEFAULT_PREFIX);

        var msg = event.getMessage();
        String content = msg.getContentRaw().toLowerCase().strip();

        if (content.startsWith(prefix)) {
            processCommand(content.substring(prefix.length()), event);
        }
    }

    private void processCommand(String command, MessageReceivedEvent event) {
        String[] split = command.trim().split(" ");
        var cmd = switch (split[0]) {
	        case "help" -> new HelpCommand();
	        case "create" -> new CreateLobbyCommand();
	        case "delete" -> new DeleteLobbyCommand();
	        case "config" -> new ConfigCommand();
	        default -> new InvalidCommand();
        };
        cmd.accept(split, event);
    }
}
