package me.draganczuk.pingus.bot;

import me.draganczuk.pingus.db.ConfigEntity;
import me.draganczuk.pingus.db.ReactionEntity;
import me.draganczuk.pingus.db.ReactionRepository;
import me.draganczuk.pingus.db.ServerRepository;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.IMentionable;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jooq.lambda.tuple.Tuple;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ReactionListener extends ListenerAdapter {
    private final ReactionRepository reactionRepository = new ReactionRepository();
    private final ServerRepository serverRepository = new ServerRepository();

    @Override
    public void onMessageReactionAdd(@Nonnull MessageReactionAddEvent event) {
        var reaction = event.getReactionEmote();
        var user = event.retrieveUser().complete();
        var server = event.getGuild().getId();
        var emoji = reaction.getName();

        if (Objects.requireNonNull(user).isBot()) {
            return;
        }

        reactionRepository.addReaction(emoji, server, user.getId());

        serverRepository.findByGuildId(server)
                .map(serverEntity -> Tuple.tuple(serverEntity.getMessageId(), serverEntity.getConfig()))
                .ifPresent(tuple -> {
                    Message message = getPreformattedMessage(server, tuple.v2());
                    event.getChannel()
                            .editMessageById(tuple.v1(), message)
                            .queue(m -> serverRepository.updateMessageId(m.getId(), server));
                });
    }

    @Override
    public void onMessageReactionRemove(@Nonnull MessageReactionRemoveEvent event) {
        var reaction = event.getReactionEmote();
        var user = event.retrieveUser().complete();
        var server = event.getGuild().getId();
        var emoji = reaction.getName();

        if (user.isBot()) {
            return;
        }

        reactionRepository.removeReaction(emoji, server, user.getId());
        serverRepository.findByGuildId(server)
                .map(serverEntity -> Tuple.tuple(serverEntity.getMessageId(), serverEntity.getConfig()))
                .ifPresent(tuple -> {
                    Message message = getPreformattedMessage(server, tuple.v2());
                    event.getChannel()
                            .editMessageById(tuple.v1, message)
                            .queue(m -> serverRepository.updateMessageId(m.getId(), server));
                });
    }

    private Message getPreformattedMessage(String server, ConfigEntity config) {
        var reactions = reactionRepository.findByGuildId(server);

        return new MessageBuilder()
                .mentionRoles(config.getRoleId())
                .append("<@&").append(config.getRoleId()).append(">\n")
                .append("A lobby has been created for today\n")
                .append("Use emotes to choose when you are available\n")
                .append(Emotes.CLOCK0).append(" - Whole day ")
                .append(formatUsers(Emotes.CLOCK0, reactions))
                .append("\n")
                .append(Emotes.CLOCK10).append(" - 10:00 ")
                .append(formatUsers(Emotes.CLOCK10, reactions))
                .append("\n")
                .append(Emotes.CLOCK11).append(" - 11:00 ")
                .append(formatUsers(Emotes.CLOCK11, reactions))
                .append("\n")
                .append(Emotes.CLOCK12).append(" - 12:00 ")
                .append(formatUsers(Emotes.CLOCK12, reactions))
                .append("\n")
                .append(Emotes.CLOCK1).append(" - 13:00 ")
                .append(formatUsers(Emotes.CLOCK1, reactions))
                .append("\n")
                .append(Emotes.CLOCK2).append(" - 14:00 ")
                .append(formatUsers(Emotes.CLOCK2, reactions))
                .append("\n")
                .append(Emotes.CLOCK3).append(" - 15:00 ")
                .append(formatUsers(Emotes.CLOCK3, reactions))
                .append("\n")
                .append(Emotes.CLOCK4).append(" - 16:00 ")
                .append(formatUsers(Emotes.CLOCK4, reactions))
                .append("\n")
                .append(Emotes.CLOCK5).append(" - 17:00 ")
                .append(formatUsers(Emotes.CLOCK5, reactions))
                .append("\n")
                .append(Emotes.CLOCK6).append(" - 18:00 ")
                .append(formatUsers(Emotes.CLOCK6, reactions))
                .append("\n")
                .append(Emotes.CLOCK7).append(" - 19:00 ")
                .append(formatUsers(Emotes.CLOCK7, reactions))
                .append("\n")
                .append(Emotes.CLOCK8).append(" - 20:00 ")
                .append(formatUsers(Emotes.CLOCK8, reactions))
                .append("\n")
                .append(Emotes.CLOCK9).append(" - 21:00 ")
                .append(formatUsers(Emotes.CLOCK9, reactions))
                .append("\n")
                .append(Emotes.X).append(" - Not today ")
                .append(formatUsers(Emotes.X, reactions))
                .build();
    }

    private String formatUsers(String emote, List<ReactionEntity> reactions) {
        return reactions.stream()
                .filter(reactionEntity -> reactionEntity.getReaction().equals(emote))
                .map(ReactionEntity::getUserId)
                .map(User::fromId)
                .map(IMentionable::getAsMention)
                .collect(Collectors.joining(", "));
    }
}
