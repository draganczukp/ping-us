package me.draganczuk.pingus.bot;

import me.draganczuk.pingus.db.*;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class GuildJoinListener extends ListenerAdapter {

    private final ConfigRepository configRepository = new ConfigRepository();
    private final ServerRepository serverRepository = new ServerRepository();
    private final ReactionRepository reactionRepository = new ReactionRepository();


    @Override
    public void onGuildJoin(@NotNull GuildJoinEvent event) {
        Guild guild = event.getGuild();

        var guildId = guild.getId();

        var config = ConfigEntity.getDefault();
        config = configRepository.create(config);

        var server = ServerEntity.getDefault(guildId, config);
        server.setId(serverRepository.create(server));

        Objects.requireNonNull(event.getGuild()
                .getDefaultChannel())
                .sendMessage("Pingus initialized. Use `p!config` to configure")
                .queue();
    }
}
