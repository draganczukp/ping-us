package me.draganczuk.pingus.db;

import lombok.Builder;
import lombok.Data;
import org.jooq.Record;

import static me.draganczuk.pingus.generated.db.Tables.CONFIG;
import static me.draganczuk.pingus.generated.db.Tables.SERVER;

@Data
@Builder
public class ServerEntity {
    private int id;

    private String serverId;

    private ConfigEntity config;

    private String messageId;

    public static ServerEntity getDefault(String guildId, ConfigEntity config) {
        return ServerEntity.builder()
                .serverId(guildId)
                .config(config)
                .build();
    }

    public static ServerEntity fromRecord(Record record) {
        var conf = ConfigEntity.builder()
                .id(record.get(CONFIG.ID))
                .prefix(record.get(CONFIG.PREFIX))
                .roleId(record.get(CONFIG.ROLE_ID))
                .build();
        return ServerEntity.builder()
                .id(record.get(SERVER.ID))
                .serverId(record.get(SERVER.SERVER_ID))
                .messageId(record.get(SERVER.MESSAGE_ID))
                .config(conf)
                .build();
    }
}
