package me.draganczuk.pingus.db;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConfigEntity {
	private int id;

	private String roleId;
	private String prefix;

	public static ConfigEntity getDefault() {
		return ConfigEntity.builder()
				.prefix("p!")
				.build();
	}
}

