package me.draganczuk.pingus.db;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReactionEntity {
    private int id;
    private String server;
    private String userId;
    private String reaction;
}
