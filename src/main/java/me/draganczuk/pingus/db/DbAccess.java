package me.draganczuk.pingus.db;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;

public class DbAccess {

	private static String connectionString;
	private static String user;
	private static String pass;

	public static void init() {
		var env = System.getenv();

		var host = env.getOrDefault("DB_HOST", "localhost");
		var port = env.getOrDefault("DB_PORT", "3306");
		user = env.getOrDefault("DB_USER", "pingus");
		pass = env.getOrDefault("DB_PASS", "pingus");
		var name = env.getOrDefault("DB_NAME", "pingus");

		connectionString = String.format("jdbc:mariadb://%s:%s/%s", host, port, name);
	}

	public static DSLContext getConnection() {
		return DSL.using(connectionString, user, pass);
	}
}
