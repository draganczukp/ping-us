package me.draganczuk.pingus.db;

import java.util.List;

import static me.draganczuk.pingus.db.DbAccess.getConnection;
import static me.draganczuk.pingus.generated.db.Tables.REACTION;

public class ReactionRepository {

	public List<ReactionEntity> findByGuildId(String guildId) {
		try (var conn = getConnection()) {
			return conn
					.selectFrom(REACTION)
					.where(REACTION.SERVER.like(guildId))
					.fetchInto(ReactionEntity.class);
		}
	}

	public void clearReactions(String guild) {
		try (var conn = getConnection()) {
			conn.deleteFrom(REACTION)
					.where(REACTION.SERVER.like(guild))
					.execute();
		}
	}

	public void addReaction(String reaction, String guild, String user) {
		try (var conn = getConnection()) {
			conn.insertInto(REACTION)
					.columns(REACTION.REACTION_, REACTION.SERVER, REACTION.USER_ID)
					.values(reaction, guild, user)
					.execute();
		}
	}

	public void removeReaction(String emoji, String server, String user) {
		try (var conn = getConnection()) {
			conn.deleteFrom(REACTION)
					.where(REACTION.REACTION_.like(emoji))
					.and(REACTION.SERVER.like(server))
					.and(REACTION.USER_ID.like(user))
					.execute();
		}
	}
}
