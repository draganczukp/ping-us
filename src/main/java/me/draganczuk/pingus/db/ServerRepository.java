package me.draganczuk.pingus.db;

import java.util.Optional;

import static me.draganczuk.pingus.db.DbAccess.getConnection;
import static me.draganczuk.pingus.generated.db.Tables.CONFIG;
import static me.draganczuk.pingus.generated.db.Tables.SERVER;

public class ServerRepository {

	public Optional<ServerEntity> getById(int id) {
		try (var conn = getConnection()) {
			return conn.select()
					.from(SERVER)
					.join(CONFIG)
					.on(SERVER.CONFIG.eq(CONFIG.ID))
					.where(SERVER.ID.eq(id))
					.fetchOptional()
					.map(ServerEntity::fromRecord);
		}
	}

	public Optional<ServerEntity> findByGuildId(String guildId) {
		try (var conn = getConnection()) {
			return conn.select()
					.from(SERVER)
					.join(CONFIG)
					.on(SERVER.CONFIG.eq(CONFIG.ID))
					.where(SERVER.SERVER_ID.like(guildId))
					.fetchOptional()
					.map(ServerEntity::fromRecord);
		}
	}

	public Integer create(ServerEntity server) {
		try (var conn = getConnection()) {
			return conn.insertInto(SERVER)
					.columns(SERVER.MESSAGE_ID, SERVER.SERVER_ID, SERVER.CONFIG)
					.values(server.getMessageId(), server.getServerId(), server.getConfig().getId())
					.returningResult(SERVER.ID)
					.fetchOne()
					.into(Integer.class);
		}
	}

	public void updateMessageId(String messageId, String guildId) {
		try (var conn = getConnection()) {
			conn.update(SERVER)
					.set(SERVER.MESSAGE_ID, messageId)
					.where(SERVER.SERVER_ID.like(guildId))
					.execute();
		}
	}

	public void purgeGuild(String guildId) {
		try (var conn = getConnection()) {
			conn.deleteFrom(SERVER)
					.where(SERVER.SERVER_ID.like(guildId))
					.execute();
		}
	}

	public void clearSavedMessageForGuild(String guildId) {
		try (var conn = getConnection()) {
			conn.update(SERVER)
					.set(SERVER.MESSAGE_ID, "")
					.where(SERVER.SERVER_ID.like(guildId))
					.execute();
		}
	}
}
