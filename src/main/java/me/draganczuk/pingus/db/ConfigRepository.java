package me.draganczuk.pingus.db;

import java.util.Optional;

import static me.draganczuk.pingus.db.DbAccess.getConnection;
import static me.draganczuk.pingus.generated.db.tables.Config.CONFIG;

public class ConfigRepository {

	public Optional<ConfigEntity> findById(Integer id) {
		try (var conn = getConnection()) {
			return conn.selectFrom(CONFIG)
					.where(CONFIG.ID.eq(id))
					.fetchOptionalInto(ConfigEntity.class);
		}
	}

	public ConfigEntity create(ConfigEntity config) {
		try (var conn = getConnection()) {
			return conn.insertInto(CONFIG)
					.columns(CONFIG.ROLE_ID, CONFIG.PREFIX)
					.values(config.getRoleId(), config.getPrefix())
					.returning()
					.fetchOne()
					.into(ConfigEntity.class);
		}
	}

	public void update(ConfigEntity config) {
		try (var conn = getConnection()) {
			conn.update(CONFIG)
					.set(CONFIG.PREFIX, config.getPrefix())
					.set(CONFIG.ROLE_ID, config.getRoleId())
					.where(CONFIG.ID.eq(config.getId()))
					.execute();
		}
	}
}
