FROM gradle:jdk15 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle shadowJar --no-daemon

FROM openjdk:15.0-slim

EXPOSE 4567

RUN mkdir /app

COPY --from=build /home/gradle/src/build/libs/ping-us-1.0-SNAPSHOT-all.jar /app/application.jar

ENTRYPOINT ["java", "-jar","/app/application.jar"]
